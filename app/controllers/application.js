import Controller from '@ember/controller';
import { action } from '@ember/object';

import { tracked } from '@glimmer/tracking';

export default class App extends Controller {
  @tracked
  hidden = true;

  @action
  toggle() {
    this.hidden = !this.hidden;
  }
}
