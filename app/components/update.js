import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class UpdateComponent extends Component {
  @tracked
  active = true;

  @action
  _onCollapsedChange() {
    this.active = !this.active;
  }
}
